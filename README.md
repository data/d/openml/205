# OpenML dataset: sleep

https://www.openml.org/d/205

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown -   
**Please cite**:   

Data from StatLib (ftp stat.cmu.edu/datasets)

 Data from which conclusions  were  drawn  in  the  article  "Sleep  in 
 Mammals: Ecological and Constitutional Correlates" by Allison, T.  and 
 Cicchetti, D. (1976), _Science_, November 12, vol. 194,  pp.  732-734. 
 Includes brain and body  weight,  life  span,  gestation  time,  time 
 sleeping, and predation and danger indices for 62 mammals.
 
 
 
 Variables below (from left to right) for Mammals Data Set:
 
 species of animal
 
 body weight in kg
 
 brain weight in g
 
 slow wave ("nondreaming") sleep (hrs/day)
 
 paradoxical ("dreaming") sleep (hrs/day)
 
 total sleep (hrs/day)  (sum of slow wave and paradoxical sleep)
 
 maximum life span (years)
 
 gestation time (days)
 
 predation index (1-5)
                 1 = minimum (least likely to be preyed upon)
                 5 = maximum (most likely to be preyed upon)
 
 sleep exposure index (1-5)
                 1 = least exposed (e.g. animal sleeps in a 
                     well-protected den)
                 5 = most exposed
 
 overall danger index (1-5)
                 (based on the above two indices and other information)
                 1 = least danger (from other animals)
                 5 = most danger (from other animals)
 
 Note: Missing values denoted by -999.0
 
 
 For more details, see
 
 Allison, Truett and Cicchetti, Domenic V. (1976), "Sleep  in  Mammals: 
 Ecological and Constitutional  Correlates",  _Science_,  November  12, 
 vol. 194, pp. 732-734.
 
 The above data set can be freely used for non-commercial purposes  and 
 can be freely distributed (permission in  writing  obtained  from  Dr. 
 Truett Allison).
 
 Submitted by Roger Johnson
 rwjohnso@silver.sdsmt.edu

 Total sleep treated as the class attribute. Attributes for slow
 wave and paradoxical sleep have been deleted. (The animal's
 name has also been deleted.)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/205) of an [OpenML dataset](https://www.openml.org/d/205). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/205/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/205/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/205/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

